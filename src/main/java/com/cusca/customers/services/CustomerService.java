package com.cusca.customers.services;

import com.cusca.customers.model.Customer;
import com.cusca.customers.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private ICustomerRepository icr;

    public List<Customer> findAllCustomer(){
        List<Customer> customers = new ArrayList<>();
        this.icr.findAll().stream().forEach(customers::add);
        return customers;
    }

    public Customer saveOrUpdate(Customer customer){
        return  this.icr.save(customer);
    }

    public Optional<Customer> findCustomerById(Long id){
        return this.icr.findById(id);
    }

    public boolean deleteCustomer(Long id){
        this.icr.deleteById(id);
        return true;
    }
}
