package com.cusca.customers.services;

import com.cusca.customers.model.Addresses;
import com.cusca.customers.model.Customer;
import com.cusca.customers.repository.IAddessesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AddressesService {

    @Autowired
    private IAddessesRepository iar;

    public List<Addresses> findAllAddresses(){
        List<Addresses> addresses = new ArrayList<>();
        this.iar.findAll().stream().forEach(addresses::add);
        return addresses;
    }

    public Addresses saveOrUpdate(Addresses product){
        return  this.iar.save(product);
    }

    public Optional<Addresses> findAddressesById(Long id){
        return this.iar.findById(id);
    }

    public boolean deleteAddresses(Long id){
        this.iar.deleteById(id);
        return true;
    }
}
