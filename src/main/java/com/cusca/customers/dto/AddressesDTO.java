package com.cusca.customers.dto;

import com.cusca.customers.model.Customer;
import lombok.Data;

@Data
public class AddressesDTO {

    private Long id;
    private Customer customer;
    private String address;
}

