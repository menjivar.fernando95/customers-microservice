package com.cusca.customers.dto;

import com.cusca.customers.model.Addresses;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class CustomerDTO {

    private Long id;
    private String name;
    private String lastname;
    private String phone;
    private String email;
    private List<Addresses> addresses;


}
