package com.cusca.customers.api;

import com.cusca.customers.dto.CustomerDTO;
import com.cusca.customers.model.Addresses;
import com.cusca.customers.model.Customer;
import com.cusca.customers.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/customers")
public class CustomerRest {

    @Autowired
    private CustomerService cs;


    @GetMapping
    public ResponseEntity<List<Customer>> getAllCustomers(){
        try{
            return new ResponseEntity<>(this.cs.findAllCustomer(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable("id") Long id){
        try{
            Optional<Customer> customer = this.cs.findCustomerById(id);
            if(customer.isPresent()){
                return new ResponseEntity<>(customer.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }

        }catch(Exception e){
            return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping
    public ResponseEntity<Customer> save(@RequestBody CustomerDTO customer){
        try{
            System.out.println(customer);
            var c = new Customer();
            System.out.println("Objecto dto" + customer);
            c.setId(customer.getId());
            c.setName(customer.getName());
            c.setLastname(customer.getLastname());
            c.setPhone(customer.getPhone());
            c.setEmail(customer.getEmail());
            c.setAddresses(customer.getAddresses());
            return new ResponseEntity<>( this.cs.saveOrUpdate(c), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCustomer(@PathVariable("id") Long id){
        try{
            this.cs.deleteCustomer(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
