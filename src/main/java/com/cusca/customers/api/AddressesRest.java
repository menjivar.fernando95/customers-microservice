package com.cusca.customers.api;

import com.cusca.customers.dto.AddressesDTO;
import com.cusca.customers.dto.CustomerDTO;
import com.cusca.customers.model.Addresses;
import com.cusca.customers.model.Customer;
import com.cusca.customers.services.AddressesService;
import com.cusca.customers.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/addresses")
public class AddressesRest {

    @Autowired
    private AddressesService as;


    @GetMapping
    public ResponseEntity<List<Addresses>> getAllAddresses(){
        try{
            return new ResponseEntity<List<Addresses>>(this.as.findAllAddresses(), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{id}")
    public ResponseEntity<Addresses> getAddresess(@PathVariable("id") Long id){
        try{
            Optional<Addresses> addr = this.as.findAddressesById(id);
            if(addr.isPresent()){
                return new ResponseEntity<>(addr.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }

        }catch(Exception e){
            return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping
    public ResponseEntity<Addresses> save(@RequestBody AddressesDTO addr){
        try{

            var a = new Addresses();
            var c = new Customer();
            c.setId(addr.getCustomer().getId());
            c.setName(addr.getCustomer().getName());
            c.setLastname(addr.getCustomer().getLastname());
            c.setPhone(addr.getCustomer().getPhone());
            c.setEmail(addr.getCustomer().getEmail());
            a.setId(addr.getId());
            a.setCustomer(c);
            a.setAddress(addr.getAddress());

            return new ResponseEntity<>( this.as.saveOrUpdate(a), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCustomer(@PathVariable("id") Long id){
        try{
            this.as.deleteAddresses(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
