package com.cusca.customers.repository;

import com.cusca.customers.model.Addresses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAddessesRepository extends JpaRepository<Addresses, Long> {
}
