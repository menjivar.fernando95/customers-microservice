package com.cusca.customers.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Addresses", schema = "public")
public class Addresses implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY )
    @JoinColumn(name = "customer_id", nullable = false, updatable = false)
    @JsonIgnore
    private Customer customer;

    @Column(name = "address")
    private String address;


}
