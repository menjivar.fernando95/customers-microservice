package com.cusca.customers.api;

import com.cusca.customers.dto.CustomerDTO;
import com.cusca.customers.model.Addresses;
import com.cusca.customers.model.Customer;
import com.cusca.customers.services.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
@RunWith(SpringRunner.class)
@WebMvcTest(CustomerRest.class)
class CustomerRestTest {
/*
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CustomerService cs;

    Customer x;
    Customer y;
    CustomerDTO z;
    Customer zx;

    List<Customer> customerList;

    List<Addresses>  a;

    List<Addresses>  b;

    List<Addresses> za;

    List<Addresses> zb;

    List<Addresses> zax;


    @BeforeEach
    void setUp() {
        x = new Customer();
        x.setId(1L);x.setName("Fernando");x.setLastname("Menjivar");x.setPhone("76194652");x.setEmail("fer@gmail.com");
        Addresses addr = new Addresses();
        addr.setId(1L);addr.setAddress("chalchuapa");addr.setCustomer(x);
        Addresses addr2 = new Addresses();
        addr2.setId(2L);addr2.setAddress("santa ana");addr2.setCustomer(x);
        a  = new ArrayList<>();
        a.add(addr);
        a.add(addr2);
        x.setAddresses(a);

        y = new Customer();
        y.setId(2L);y.setName("Fernando2");y.setLastname("Menjivar2");y.setPhone("76194653");y.setEmail("fer2@gmail.com");
        Addresses addrb = new Addresses();addr.setId(3L);addr.setAddress("chalchuapa2");addr.setCustomer(y);
        Addresses addrb2 = new Addresses();addr2.setId(4L);addr2.setAddress("santa ana2");addr2.setCustomer(y);
        b = new ArrayList<>();
        b.add(addrb);
        b.add(addrb2);
        y.setAddresses(b);

        z = new CustomerDTO();
        z.setId(3L);z.setName("Fernando3");z.setLastname("Menjivar3");z.setPhone("76194654");z.setEmail("fer3@gmail.com");


        zx = new Customer();
        zx.setId(3L);zx.setName("Fernando3");zx.setLastname("Menjivar3");zx.setPhone("76194654");zx.setEmail("fer3@gmail.com");zx.setAddresses(null);


        customerList = new ArrayList<Customer>();
        customerList.add(x);
        customerList.add(y);

    }

    @AfterEach
    void tearDown(){
    }

    @Test
    void getAllCustomer() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(x);
        String p2 = objectMapper.writeValueAsString(y);
        given(this.cs.findAllCustomer()).willReturn(customerList);

        this.mvc.perform(get("/customers").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[" + p1 + "," + p2 + "]"));
    }

    @Test
    void getCustomer() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(x);

        given(this.cs.findCustomerById(x.getId())).willReturn(customerList.stream().findFirst());
        this.mvc.perform(get("/customers/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(p1));
    }

    @Test
    void save() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String p1 = objectMapper.writeValueAsString(z);
        String p2 = objectMapper.writeValueAsString(zx);
        given(this.cs.saveOrUpdate(zx)).willReturn(zx);
        this.mvc.perform(post("/customers")
                        .content(p1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(p2));
    }

    @Test
    void deleteCustomer() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        given(this.cs.deleteCustomer(x.getId())).willReturn(true);
        this.mvc.perform(delete("/customers/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

 */
}