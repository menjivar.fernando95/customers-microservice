package com.cusca.customers.services;

import com.cusca.customers.model.Customer;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource("classpath:applicationtest.properties")
@Sql(statements = "insert into Customer(name,lastname,phone,email) values('fernando','menjivar', '76194652', 'fer@gmail.com')")
class CustomerServiceTest {
/*
    @Autowired
    CustomerService cs;

    @Test
    void findAllCustomer() {
        Assert.assertNotNull(this.cs.findAllCustomer());
    }

    @Test
    void saveOrUpdate() {
        Customer y = new Customer();
        y.setName("fernando2");
        y.setLastname("menjivar2");
        y.setPhone("76194653");
        y.setEmail("fer2@gmail.com");
        Customer x = this.cs.saveOrUpdate(y);
        Assert.assertEquals(x, y);
    }

    @Test
    void findCustomerById() {
        Customer y = new Customer();
        y.setId(1L);
        y.setName("fernando");
        y.setLastname("menjivar");
        y.setPhone("76194652");
        y.setEmail("fer@gmail.com");
        Optional<Customer> result = this.cs.findCustomerById(1L);
        Assert.assertEquals(y.getId(),result.get().getId());
        Assert.assertEquals(y.getName(),result.get().getName());
        Assert.assertEquals(y.getLastname(),result.get().getLastname());
        Assert.assertEquals(y.getPhone(),result.get().getPhone());
        Assert.assertEquals(y.getEmail(),result.get().getEmail());



    }


    @Test
    void deleteCustomer() {
    }

 */
}